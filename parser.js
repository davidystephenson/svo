var fs = require('fs');
pathToFile = process.argv[2];

var file, lines;

var parseFile = function() {
  fs.readFile(pathToFile, function(error, data) {
    file = data.toString();
    lines = file.split('\n');

    for (i = 0; i < lines.length; i ++) {
      parseLine(lines[i]);
    }
  });
};

var lineNumber = 0;

var parseLine = function(line) {
  console.log('line', lineNumber, 'test:', line);
  
  lineNumber ++;
};

parseFile(pathToFile);
